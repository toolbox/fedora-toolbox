FROM registry.fedoraproject.org/fedora-toolbox:latest

COPY extra-packages /
RUN dnf -y upgrade && \
    dnf -y install $(<extra-packages) --allowerasing && \
    dnf clean all && \
    rm /extra-packages
